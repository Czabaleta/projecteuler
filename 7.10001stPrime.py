n = int(input("Enter the last integer number you want to check: "))


def sieve_eratosthenes(n):

    count = 0
    multiples = set()

    for i in range(2, n+1):

        if i not in multiples:  # Entonces es primo
            multiples.update(range(i*i, n+1, i))
            count += 1
            print("Prime Number", i, end=" --> ")
            print("#", count)

            if count == 10001:
                print("\nPrime Number", count, "is -->", i)
                break


sieve_eratosthenes(n)