a = 100


def sumcuad(a):

    suma = 0

    for i in range(0, a):
        cuadrado = i * i
        suma = suma + cuadrado

    return suma


def cuadsum(a):

    suma = 0
    cuadrado = 0

    for i in range(0, a):
        suma = suma + i
        cuadrado = suma * suma

    return cuadrado


print("\n")
print("Diferencia entre la suma del cuadrado y el cuadrado de la suma")
print(cuadsum(a), "-", sumcuad(a), "=", (cuadsum(a) - sumcuad(a)))